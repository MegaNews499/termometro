## Projeto de Monitoramento de Temperatura de Datacenter usando Arduino

Este projeto consiste em um sistema de monitoramento de temperatura para um datacenter utilizando o Arduino e o sensor de temperatura LM35, com integra��o ao Zabbix por meio de HTTP.

### Componentes Utilizados

- Arduino Uno (ou compat�vel)
- Shield para Arduino - Padawan da RoboCore


### Descri��o do Projeto

O objetivo deste projeto � monitorar a temperatura de um datacenter e enviar os dados coletados para o Zabbix, uma ferramenta de monitoramento de rede. O sensor de temperatura LM35 � conectado ao Arduino por meio do Shield Padawan da RoboCore, que facilita a conex�o de sensores anal�gicos.

O projeto consiste em utilizar o Arduino e o sensor de temperatura LM35 para ler a temperatura do datacenter. Os dados s�o enviados via porta serial para uma aplica��o em Go, que os disponibiliza para o Zabbix por meio de requisi��es HTTP. O Zabbix processa e armazena os dados, permitindo o monitoramento da temperatura do datacenter e o recebimento de alertas em caso de varia��es significativas.


### Uso

1. Certifique-se de que o Arduino est� devidamente conectado ao sensor de temperatura e configurado.
2. Execute o programa no Arduino.
3. O Arduino ir� coletar a temperatura do sensor e enviar os dados para o Zabbix periodicamente.
4. Verifique a interface do Zabbix para monitorar a temperatura do datacenter e receber alertas, se configurados.